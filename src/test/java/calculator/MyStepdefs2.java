package calculator;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class MyStepdefs2 {
    private Calculator calculator;
    private int input;
    private double result;

    @Before
    public void before() {
        calculator = new Calculator();
    }

    @Given("^One input value, (-?[1-9]\\d*|0)$")
    public void oneInputValue(int arg0) {
        input = arg0;
    }

    @When("^I op ([a-z]+) the value$")
    public void iOpTheValue(String arg0) {
        if (arg0.equals("sqrt")){
            result = calculator.sqrt(input);
        }
        else if (arg0.equals("rvs")){
            result = calculator.rvs(input);
        }
    }
    @Then("^I expect the results ([-?1-9\\d*|0].*\\d*)$")
    public void iExpectTheResults(double arg0) {
        Assert.assertEquals(arg0, result, 0.001);
    }
}
