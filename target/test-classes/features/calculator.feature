@tag
Feature: Calculator

#  Scenario: add two numbers
#    Given Two input values, 4 and 2
#    When I add the two values
#    Then I expect the result 6
#
#
#    Scenario Outline: add two numbers
#      Given Two input values, <first> and <second>
#      When I add the two values
#      Then I expect the result <result>
#
#      Examples:
#      | first  | second | result |
#      | 1 | 12 |        13             |
#      | -1 | 6 |         5             |
#      | 2 | 2  |          4             |

      Scenario: sqrt or rvs input number
        Given One input value, 4
        When I op sqrt the value
        Then I expect the results 2

        Given One input value, 4
        When I op rvs the value
        Then I expect the results 0.25

        Scenario Outline: sqrt or rvs input number
          Given One input value, <input>
          When I op <operator> the value
          Then I expect the results <result>

          Examples:
          |input | operator | result |
          | 4 | rvs | 0.2500 |
          | 2 | sqrt | 1.4142 |
          | 8 | rvs | 0.1250 |
          | 10 | rvs | 0.1000 |
          | 16 | sqrt | 4.0000 |
          | 49 | sqrt | 7.0000 |
          | -10 | sqrt | -1 |
          | -7 | rvs | -0.1429 |